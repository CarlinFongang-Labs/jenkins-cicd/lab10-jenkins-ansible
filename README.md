# Jenkins | Jenkins pipeline CI/CD with Ansible 

------------

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com

><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Context
In this lab, we will integrate Ansible with Jenkins to deploy and configure an application on AWS EC2 based on Ubuntu 22.04.

## Requirements
* [Docker](https://docs.docker.com/get-docker/)
* [Docker Compose](https://docs.docker.com/compose/install/)
* [Repository](https://github.com/CarlinFongang/jenkins-pipeline-ansible/tree/master)

Note: IP addresses and usernames are declared in plain text because it's a temporary server. In the case of a permanent server, these values should be parameterized as done with security keys later in this lab.

## Creating a Client Machine
Create a client machine and note the IP address to be entered in the hosts.yml file.
`3.231.162.90`
>![alt text](img/image-8.png)

## Jenkins Configuration
### Installation of Docker Pipeline Plugin
This plugin allows Jenkins to mount workspaces in Docker containers to execute utilities specified in the Jenkins pipeline (Jenkinsfile).

Dashboard > Manage Jenkins > Plugins > Docker Pipeline
Execute the installation and restart Jenkins.
>![alt text](img/image.png)

### Installation of the GitHub Integration Plugin
Install the GitHub Integration plugin following the [procedure here](https://gitlab.com/CarlinFongang-Labs/jenkins-cicd/lab5-deploiement#configuring-github-integration-with-jenkins).

### Configuration of Credentials
Dashboard > Manage Jenkins > Credentials > (Global)
1. Configure a key (`Secret: devops`) and the key name (`ID: vaultkey`), it must match the value specified in the Jenkinsfile.
>![alt text](img/image-2.png)

2. Creation of the SSH key pair
ID: `ec2_ssh_key` File: key_file.pem
>![alt text](img/image-3.png)

3. Creation of the sudo password
Secret `admin` ID: `sudopass`
>![alt text](img/image-4.png)



### Pipeline Creation
Dashboard > New Item > webapp > Ok

1. Project repository information
>![alt text](img/image-5.png)

2. Pipeline configuration
>![alt text](img/image-6.png)
*Repository to clone*

>![alt text](img/image-7.png)
*Branch and configuration file to consider in the repository*

## Deployment on EC2 with Ansible playbook
### Ansible Configuration File (ansible.cfg)
````
[defaults]
host_key_checking = False
retry_files_enabled = False

[privilege_escalation]
become = True
````
The Ansible configuration file (ansible.cfg) defines default settings for Ansible executions. It disables host key checking and retry files. Additionally, it enables privilege escalation for Ansible tasks by setting `become` to True in the `privilege_escalation` section. This allows Ansible to execute tasks with superuser privileges when necessary.

### hosts.yml File
```yaml
---
all:
  vars:
    ansible_ssh_common_args: '-o StrictHostKeyChecking=no'
  children:
    prod:
      hosts:
        ec2_instance:
          ansible_host: 3.231.162.90
          ansible_user: ubuntu
```

The Ansible inventory file (hosts.yml) organizes hosts into groups. It defines common variables for all hosts, disabling strict host key checking for SSH. It creates a group named "prod" containing a host named "ec2_instance" with a specific IP address and defined SSH user. This allows Ansible to connect to the EC2 instance using the Ubuntu user without strict host key checking.

### prod.yml File
```yaml
---
ansible_user: "{{ ansible_user_vault }}"
ansible_password: "{{ ansible_password_vault }}"
```
This Ansible code sets the variables `ansible_user` and `ansible_password` using values stored in Ansible vaults. The `ansible_user_vault` and `ansible_password_vault` variables contain authentication information encrypted in vaults. Using vaults helps secure sensitive information such as credentials in Ansible environments. These values are used for authentication when connecting to remote hosts during the execution of Ansible tasks.

### deploy.yml File
```yaml
---
- name: "Apache installation using Docker on Ubuntu EC2"
  hosts: prod
  become: true
  vars_files:
    - files/secrets/credentials.yml
  tasks:
    - name: Install required packages
      apt:
        name:
          - apt-transport-https
          - ca-certificates
          - curl
          - software-properties-common
        state: present
        update_cache: true

    - name: Ensure Docker service is running
      service:
        name: docker
        state: started
        enabled: yes

    - name: Create Apache container
      community.docker.docker_container:
        name: webapp
        image: httpd
        ports:
          - "80:80"
        state: started
        restart_policy: always
```

This Ansible playbook orchestrates the deployment of an Apache container on an Ubuntu EC2 instance. It uses Ansible modules to install necessary packages, ensure the Docker service is running, and create an Apache container from the httpd image with automatic restart in case of failure. Variables are loaded from the `credentials.yml` file, and the playbook is targeted at hosts in the "prod" group.

## Create an EC2 Instance for Deployment
Deploy an EC2 instance with Docker installed on it.
[How to Install Docker: Explanation](https://gitlab.com/CarlinFongang-Labs/docker-labs/docker-install)
>![alt text](img/image-8.png)

Provide the IP address of the new machine in the hosts.yml file.

## Output of Jenkins Pipeline Execution
>![alt text](img/image-9.png)

## Deployment Output on the EC2 Instance
>![alt text](img/image-10.png)

The application is successfully deployed and available.






















